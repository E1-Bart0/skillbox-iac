terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}


variable "digitalocean_token" {}

provider "digitalocean" {
  token = var.digitalocean_token
}

resource "digitalocean_ssh_key" "default" {
  name       = "ssh key for DO"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "digitalocean_images" "ubuntu" {
  filter {
    key    = "distribution"
    values = ["Ubuntu"]
  }

  filter {
    key    = "regions"
    values = ["fra1"]
  }
}
resource "digitalocean_droplet" "worker" {
  count    = 2
  image    = data.digitalocean_images.ubuntu.images[0].slug
  name     = "worker-${count.index}"
  region   = "fra1"
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
  tags     = ["skillbox-2"]
}

resource "digitalocean_loadbalancer" "web" {
  name   = "loadbalancer-1"
  region = "fra1"
  droplet_ids = digitalocean_droplet.worker.*.id

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

}

output "my_web_site_ip" {
  description = "IP address"
  value       = digitalocean_droplet.worker.*.ipv4_address
}

output "loadbalancer_url" {
  description = "URL For loadbalancer"
  value       = digitalocean_loadbalancer.web.ip
}
