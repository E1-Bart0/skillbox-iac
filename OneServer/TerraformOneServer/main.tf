terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}


variable "digitalocean_token" {}

provider "digitalocean" {
  token = var.digitalocean_token
}

resource "digitalocean_ssh_key" "default" {
  name       = "ssh key for DO"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "digitalocean_images" "ubuntu" {
  filter {
    key    = "distribution"
    values = ["Ubuntu"]
  }

  filter {
    key   = "regions"
    values = ["fra1"]
  }
}

resource "digitalocean_droplet" "node1" {
  image    = data.digitalocean_images.ubuntu.images[0].slug
  name     = "node1"
  region   = "fra1"
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
  tags     = ["skillbox"]
}

output "my_web_site_ip" {
  description = "IP address"
  value       = digitalocean_droplet.node1.ipv4_address
}